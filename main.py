import json
import numpy as np
import pandas as pd
import csv
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy import text

#opening JSON file
y= pd.read_json('ForcesOfTheImperiumCollectorSEdition_40K.json')
print(y)

df = pd.read_excel("decks.xlsx", sheet_name="Esika")
print(df)

x=pd.read_csv('recettes.csv')
print(x)

#t=pd.read_xml('dВclaration 1.xml')
#print(t)

json_data = [
    {
        "product": {
            "product_name": "computer",
            "price": 1200
        },
        "store": {
            "store_number": 77,
            "store_city": "London"
        },
        "time": {
            "opening_hours": "8-18",
            "opening_days": "Mon-Fri"
        }
    },
    {
        "product": {
            "product_name": "printer",
            "price": 200
        },
        "store": {
            "store_number": 77,
            "store_city": "London"
        },
        "time": {
            "opening_hours": "8-18",
            "opening_days": "Mon-Fri"
        }
    }
]

# Json to DataFrame
df = pd.json_normalize(json_data)
#4
# DataFrame to Excel
excel_filename = 'json_data_to_excel.xlsx'
df.to_excel(excel_filename, index=False)
#7
#XML to Excel
data = pd.read_xml('travail.xml')
data.to_excel('customers.xlsx', index=False)
#17
#Excel to XML
data_1 = pd.read_excel('customers.xlsx')
data_1.to_xml('dВclaration 1.xml', index=False)

# Json to DataFrame
df = pd.json_normalize(json_data)

#1
# DataFrame to XML
xml_filename = 'Report 2 concerne Mustermann.xml'
df.to_xml(xml_filename, index=False)

# Json to DataFrame
df = pd.json_normalize(json_data)

#1
# DataFrame to XML
csv_filename = 'grouped_extract_A.csv'
df.to_csv(csv_filename, index=False)

# Json to DataFrame
df = pd.json_normalize(json_data)

#5
#XML to CSV
data = pd.read_xml('dВclaration 1.xml')
data.to_csv('recettes.csv', index=False)

#8
#CSV to JSON
data = pd.read_csv('recettes.csv')
data.to_json('ForcesOfTheImperiumCollectorSEdition_40K.json', index=False)

#9
#CSV to XML
data = pd.read_csv('recettes.csv')
data.to_json('dВclaration 1.xml', index=False)

#11
#CSV to Excel
data = pd.read_csv('recettes.csv')
data.to_excel('EXAMEN BTS.xlsx', index=False)
#16
data_2 = pd.read_excel('Untitled 1.xlsx')
data_2.to_json('credentials.json', index=False)

#17
#data_3 = pd.read_excel('pivot-tables.xlsx')
#data_3.to_xml('gfg.xml', index=False)

#18 
data_3 = pd.read_excel('pivot-tables.xlsx')
data_3.to_csv('pays.csv', index=False)
#12 
#engine = create_engine('sqlite:///tutorial.db', echo=False)
#df = pd.DataFrame({'name' : ['User 1', 'User 2', 'User 3']})
#df.to_sql(name='users', con=engine)
#with engine.connect() as conn:
   #conn.execute(text("SELECT * FROM users")).fetchall()

import pandas as pd
from sqlalchemy import create_engine

# Create a SQLite engine
#engine = create_engine('sqlite:///tutorial.db')

# Read the CSV file into a DataFrame
#df = pd.read_csv('pays.csv')

# Write the data to a SQLite table
#df.to_sql('my_table', con=engine, if_exists='replace', index=False)
# Create a SQLite engine
#engine = create_engine('sqlite:///tutorial.db')

# Read the CSV file into a DataFrame
#df = pd.read_xml('launcher manifest.xml')

# Write the data to a SQLite table
#df.to_sql('ma_table', con=engine, if_exists='replace', index=False)

# Create a SQLite engine
engine = create_engine('sqlite:///tutorial.db')

# Read the CSV file into a DataFrame
df = pd.read_excel('pivot-tables.xlsx')

# Write the data to a SQLite table
df.to_sql('table', con=engine, if_exists='replace', index=False)
